//miniX4 - Data capture by Linus Lentz
//with code from class05
//powered with clmtrackr by Audun Mathias Øygard

let ctracker;
let capture;

function setup() {
  //create canvas
  createCanvas(1280, 960);
  background(0);

  //setup web cam capture
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();

  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

  //text
  fill(255);
  textSize(18)
  text('Automatic portrait', 560, 200)
  text('press F5 or refresh to take a new picture', 490, 800);
  text('try to reload the page with your face not visible', 470, 850);
}

function draw() {
  //drawing the captured video
  image(capture, 320, 240, 640, 480);

  //purposefully crashing the program when a face is recognised
let positions = ctracker.getCurrentPosition();
if (positions.length) {
  triggerfoto.position(); //the program is unable to find this undefined variable, which makes the video stream freeze
  }
}
