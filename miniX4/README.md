
<h2>README for miniX4 by Linus Lentz</h2>

![](miniX4.gif)

Run miniX: https://lentz.gitlab.io/aestheticprogramming/miniX4/

Source code: https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX4/sketch.js

credits go to aestheticprogramming class05 and Audun Mathias Øygard's and his "clmtrackr" shown to us in Winnie Soon's Aesthetic Programming class: https://github.com/auduno/clmtrackr

<h4>The assignment</h4>
This week we were introduced to data capture methods in p5.js, including web camera,
microphone, keyboard- and mouse inputs, as well as face-tracking by Audun Mathias Øygard.
My miniX for this week was inspired by the fact that data capture always seems to be connected to data privacy. Originally I planned to use microphone capture, but I was not able to get that working properly,
therefore I switched to the more or less properly working webcam capture.
The program will "take a picture" when it recognizes a face. This means the user sees a videofeed,
which promptly stops when their face is recognized. This also the reason the program is called "Automatic Portrait" instead of "Automatic Photograph" as it only will make portrait pictures. This prototype is meant to showcase,
how camera software could potentially filter out people in a recording and only focus on specific
individuals whose faces were fed beforehand to the program. A pretty scary thought, as this would mean there is a software that can track ones location everywhere there is a camera pointed at you it has access to.
On a laptop this could also be used as a kind of surveillance software, which only captures video when a face is in the picture, meaning a stalker would have less to worry about bandwidth and storage capability.
But you could also just have fun with it by making selfies.

<h4>The Code</h4>
The code for this week is pretty simple, but also very advanced at the same time, as it makes use of the clmtrackr.js library to enable face tracking.
At first two variables are declared, "ctracker" and "capture", the first is important for the face tracking to work (or not... ), while the other one is used for enabling the webcam capture. After putting in the correct functions for them to work in "setup()", the interesting part comes. The program is purposefully crashed when calling in the face tracking, so that the videofeed freezes, making it look like a photograph was taken. This approach is not the cleanest, as I have no further control over the executable after the crash. I tried to include the keypress input capture for resetting this part, but that did of course not work as the program is unresponsive. The reason for the crash is simple, I put in a made up position variable in the "getCurrentPosition()" function, which can't be resolved and ends in an error. The reason for the crash only happening when a face is identified, is because the particular function is only called in that particular scenario, that is at least what think happens.

<h4>Transmediale 2015 proposal: Webcam Surveillance gone wrong</h4>
Are you getting tired of hackers trying to steal information or a secret government organisation keeping watch on your „suspicious“ activity? No problem for this anti-spyware. With this software, whenever your face it detected, the whole thing crashes and stops responding. That should put a halt on anyone trying to get a peek. Of course you won’t be able to use your webcam normally either… Better get the old trusty tape instead.


<h4>Conclusion</h4>
This weeks miniX4 ended up pretty interesting, as it cuts into some sensitive topics regarding data and privacy, and is also a bit fun on the technical level.
I did not think I would at one point include a severy bug as a feature in one of my miniXs. Actually the bug inspired me to take it in the direction it went, and I am really suprised by that. Anyways I will experiment more with sound as soon as I get that working correctly.
Regarding the theme "Capture all" miniX probably does not capture enough different kinds of data to be classified as it. The data it does capture is a snapshot of whatever face it detects first. Disregarding that we allowed webcam capture before opening the site, this „automatic portrait“ was done without ones consent to it. The thought of someone having a big portrait of ones face is not really calming, especially regarding what can be done with ones information and data nowadays without conscious consent, because who really reads EULA? The image could be used in advertisement, it could be fed into face recognition algorithms (Software Studies) which would classify it as being some sort of stereotype. Identity theft is huge concern to, what is hindering the perpetrator in setting up a Facebook or Twitter Account with your information? Though if were going to be realistic, those are probably easily available on the web regardless.



Reference

Transmedial call for works 2015: https://archive.transmediale.de/content/call-for-works-2015

Aesthetic programming class: https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class05
