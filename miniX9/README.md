## miniX9

![](platformer-pitch.png)

![](questionaire-pitch.png)

Linus' flowchart: 
https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX9/miniX9_flowchart.png

Jeppe's flowchart:
https://whimsical.com/minix7-flowchart-8W5SofEiRzQZt2zYcsE391

Nicolai's flowchart:
https://whimsical.com/minix9-TXaFAuf26o8eopyaUvRk1Q

Rikkes flowchart:
https://whimsical.com/rikke-s-flowchart-Ky9qVVpdD9dAZhE4e8ByxF

Mortens flowchart:
https://whimsical.com/morten-s-flowchart-C18qviUhscAG7TjbUVqPji

Flowchart of our first idea for final miniX:
https://whimsical.com/platformer-EnpUk9mQUF1r8X7Q5qfAmW

Our second idea:
https://whimsical.com/group-flowchart-3a1K2iKJvuyqPgv8vvToV2

### Our ideas
This idea is about a sort of platformer game, where you first have a set of levels that you play through where you have to pick up items to complete each level. Once every level is completed, a Boss level loads where you have to defeat the boss. Here you either win the game by defeating the Boss, or lose it completely, as the program just ends if you lose. 
The idea behind the themes of the game would be to have the player try and fight some sort of oppression, by first collecting some “resources” that would help in the fight against the oppressor(The Boss), and then to use these items to aid you in the fight against the Boss.  

Some of the difficulty comes from who you are trying to communicate with, with your flow chart. If the objective of the flowchart is to just communicate with a layperson, using simpler, non-coding language and not getting too fancy with the flowchart blocks might be better. Though if the flow chart is meant for peers, upping the complexity can be beneficial for communication, like using actual syntax names, descriptive text, fancier flowchart blocks etc. Though even for peers who have some level of coding experience, the simpler flowchart can be very useful in an ideation phase, in order to make sure everyone understands the main objectives of the program. A simpler flowchart can then be made more complex, where everyone should then be able to follow along with a project, kinda like starting out in the shallow end of a pool before just jumping in at the deep end.  

### Technical challenges
The two ideas face different technical challenges. The platformer idea requires us to code some sort of gravity system that allows the character to move around the level in a satisfying way, while at the same time limiting the amount of jumps possible to ensure that the player can’t “cheat” the level (double jump etc.) Luckily we’ve had members who have previously worked on something similar and we know that people like Daniel Shiffman have videos on the topic, so it’d definitely be doable to set up a gravity system that works, although probably hard to do a very good one. We’d also have to figure out a way to make the levels switch, either by having collectibles that are needed to “beat” the level or have a point at the end of the level that makes the transition happen. The movement of the character and how to insert the videos (intro/ending) clips seems rather simple, so it is mainly the core mechanics that could challenge us a lot. 

Given that our second idea is based on a previous program of ours, the coding aspect of it seems very doable, but we would loads more questions as well as a more precise “result”. Deciding on what the questions should be about, that alone seems difficult. There is more of a conceptual challenge due to us already having code from similar program, so it’s more about planning the program down to the minor details. What should it be about, what should the questions be, how many “points” do each answer give and what outcomes should there be? These are just some of the many challenges involving this idea. 

### How is this useful?
The flowcharts we produced individually, and as a group, are useful for analyzing the main components of our programs. Separating and recognizing the programs processes, makes it easier to visualize and see which parts are important and which less so. Thereby giving us the opportunity to better streamline the program, making it more efficient, and thereby less resource consuming. This means both a smaller ecological footprint(1) and more efficient coding in the future. We are convinced that the lessons learned through analyzing our code, makes us able to write more efficient code in future projects. Other than helping in further developent of software, the simple structure and form of explanation makes the programs inner workings more accessible to people less or unexperienced in code. It also circumvents the phenomenon of the “Black Box”(2) mentioned in “Uncertain Archives Critical Keywords for Big Data” by Gipson, Corry and Noble. It describes the lack of transparency to the way information are processed by an algorithm. Through our flowcharts we make it clear to most people how our programs work.

#### Literature
(1)
Sean Cubitt, Finite Media: Environmental Implications of Digital Technologies. Durham: Duke University Press 2016, pp. 16-26 and 140-146.

(2)
Brooklyne Gipson, Frances Corry, and Safiya Umoja Noble, “Intersectionality” in Uncertain Archives Critical Keywords for Big Data. Eds. Nanna Bonde Thylstrup, Daniela Agostinho, Annie Ring, Catherine D'Ignazio and Kristin Veel. Cambridge, MA: The MIT Press 2021, 305-312.
