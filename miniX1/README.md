![](showcase.gif)

For my first miniX i decided to make a drawing of a Nintendo GameBoy. At first I wanted the drawing to be interactive, but because I was not able to find a way to enable it in a way I would be satisfied yet, I chose instead to show gameplay. To the left of it there stands written "The future of Retro Gaming?".

I chose this as a title, because we just picked up the topic of preservation and maintenance of old tech and software that comes with it, in Software Studies class. This was particularly interesting to me, as I collector old game system like the GameBoy. As these system are getting close to 40 years old in some circumstances the electronics inside these are starting to fail in several places. How would I be able to play them in in, lets say, another 40 years? At some point the electronics will fail no matter what. At that point the only way to play these old games would be to emulate them. Currently there are two types of emulation used for this: the prominent software emulation and the lesser known hardware emulation. Both are very different in how they operate, but in an optimal enviroment they will give out virtually the same result. As hardware emulation can only be done with special chipsets, so called "FPGA"s, I will focus on software emulation for now.

The closest thing I could compare my first coding experience too, would be drawing. Like drawing with a new kind of pencil or brush. When wanting to put something on the screen, I had to think which coordinates would put the object where on the screen. This reminds me of how I learned coordinate systems in math class at school. With P5 the reading of code gets rather easy, because the function have names clearly corresponding to what they do. Take fill() and ellipse() for example. By only reading these two I can already think of what they will do in my program. The difficulty in reading code comes more from the values inside the functions. For the most part it is just numbers, and is not self explanatory which number does what. I learned that the first to values almost always correspond to x- and y-coordinates., but because every value looks the same, it gets kind of hard to read through. This is also were the difficulty in modifying codes comes from. If the code is not written by myself it gets hard to see exactly which value does what.

I feel that coding takes a lot more precision than writing standard text. When I forget a comma in writing, the text will still be understandable, most of the time that is at least. When I forgot a comma in code, the whole program could potentially stop working. Would there not be an option in browsers to look up a console that helps identify errors, then coding would be really frustrating to me. In terms of reading code, I feel like it is more like reading someones notes than reading a text. I dont even feel like that analogy is entirely accurate, as there is not really a form of „source code“ in normal text. In literature there is only one dimension, but there are two in coding. The code and its execution. One can guess what the code looks like by analyzing the execution. This could maybe be compared to how one can read a text „between the lines“, where one could try to guess what the author tries to say with a text.

For me coding and programming is an extension of expression like writing, photographing and other mediums. It can combine elements from all different mediums and arrange them new. One of the things I find most exciting about coding is that it gives me the ability to produce something that people can interact with. Where as most mediums are rather passive, as one listens to a radio show, watches television or reads a book, coding on the other hand makes it possible to integrate the user directly. Like Lauren McCarthy says at the OPENVIS Conference in 2015, it is important that people of all different backgrounds and abilities have the opportunity to make code, as coding, like reading and writing before it, is something that gives one the ability to partake in society effectively.

The miniX I made, is supposed to be a proof of concept for how emulation could look. You have the full GameBoy shown instead of just its screen, there is no question about which console is playing. Most emulators dont aknowledge the systems they are emulating in a meaningful way. My goal with a new emulator would be to include the layer of experience outside of what happens on screen as much as possible. This would include behaviour and / or the feel of the case, maybe also sounds the case itself would make?


Link to execute miniX in browser: https://lentz.gitlab.io/aestheticprogramming/miniX1/

Link to the source code: https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX1/sketch.js


Reference

Game shown: The Legend of Zelda: Link's Awakening (1993 - Nintendo)

Lauren McCarthy, “Learning While Making p5.js,” OPENVIS Conference (2015), https://youtube.com/watch?v=1k3X4DLDHdc.

https://aesthetic-programming.net/pages/1-getting-started.html#minix-runme-and-readme-28833
