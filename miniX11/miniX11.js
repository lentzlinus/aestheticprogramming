// defining variables
let story;
let qandA;
let test;
let n;
let t;
let speak;
let speakI;
let speakQ;
let speak3;
let button1;
let button2;
let button3;
let button4;
let qn; //question number
let rq; //random quip
let poor;
let pl //poor length
let patient;
let name, age, ethnicity, gender, sexuality, cookies
let ccis; //climatechangeinfluencescore
let quip;
let score;
let house1,house2,house3,house4
let job1,job2,job3,job4
let house,job
let future05,future10,future15,future20,future25
let bg //background
let bagr
let loadingloading

function preload(){ // preloading the necessary files
  story = loadJSON('story.json');
  qandA = loadJSON('Questions.json');
  patient = loadJSON('comapatientnr17867.json')
 future05=loadImage('pics/future05.gif')
 future10=loadImage('pics/future10.gif')
 future15=loadImage('pics/future15.gif')
 future20=loadImage('pics/future20.gif')
 future25=loadImage('pics/future25.gif')
 loadingloading=loadImage('pics/loadingloading.gif')
}

function setup(){
createCanvas(windowWidth,windowHeight)
n=0
t=0
qn=0
x=0
loadingScreen()

}


function loadingScreen(){
  poor=[];
  cookies=[];
  pl=poor.length

  fetch('https://literallyallyourdata.org/'+patient.GovID+'.JSON')
  .then(response=>{
    console.log(yay);
    return response.json();
  })
.then(json=>{
age = json.age;
ethnicity = json.ethnicity;
gender = json.gender;
sexuality = json.sexuality;
for (let i=0;i<json.cookies.length;i++){
  cookies[i] = json.cookies[i];
}
ccis = json.ccis;
house=json.house
jov=json.job
})
.catch(error=>{
  console.log('error');
  age = 'null'
  ethnicity = 'null'
  gender = 'null'
  sexuality = 'null'
  cookies[0]= 'null'
  ccis = 0
  house='null'
  job='null'
for (let i=0;i<1;i++){
     poor[i] = new Poor(age, ethnicity, gender, sexuality, cookies[i],ccis,house,job)
   }
  setTimeout(loadNewIntroLine,0000)
})


}
function testing(){
  console.log('poop')
}


function loadNewIntroLine(){
bg=loadingloading
x=2
if (n<story.welcome.length){
  let welcome = "story/" + story.welcome[n].audio + ".mp3"
  speakI = loadSound(welcome, speakIntro)
}
}


function speakIntro(){
  speakI.play();
  speakI.onended(speakAgain)

}

function speakAgain(){
  n++
if (story.welcome.length >n){
  setTimeout(loadNewIntroLine,1000);
}else if(story.welcome.length ==n){
  setTimeout(loadFunFacts,5000)

}
}

function draw(){
  if(x==1){
    background(bg)
    textSize(30);
    textAlign(CENTER);
    stroke(color(0, 0, 0));
    strokeWeight(4);
    fill(255, 255, 255);
    text(qandA.digitalclimatetest[qn].question, width/2, height/4)
  }else if(x==2){
    background(bg)
  }

}


function loadFunFacts(){

  if (poor[0].ccis<=20){
    bg=future05
  } else if(31<=poor[0].ccis && poor[0].ccis<=60){
    bg=future10
  }else if(61<=poor[0].ccis && poor[0].ccis<=90){
    bg=future15
  }else if(91<=poor[0].ccis && poor[0].ccis<=120){
    bg=future20
}else if(poor[0].ccis>120){
    bg=future25
}
x=2


  let funFacts = "story/" + qandA.digitalclimatetest[qn].factAudio + ".mp3"
  speakFF = loadSound(funFacts,speakFunFacts)
}

function speakFunFacts(){
  speakFF.play()
  speakFF.onended(nextQuestion,2000)
}

function nextQuestion(){
  setTimeout(loadQuestions,2000)
}
function loadQuestions(){
  let questions = "story/" + qandA.digitalclimatetest[qn].audio + ".mp3"
  speakQ = loadSound(questions,speakQuestions)
}



function speakQuestions(){
  x=1
  speakQ.play();
  textSize(30);
  textAlign(CENTER);
  stroke(color(0, 0, 0));
  strokeWeight(4);
  fill(255, 255, 255);
  text(qandA.digitalclimatetest[qn].question, width/2, height/4);
  speakQ.onended(buttons)

}



function buttons(){

  push()
  button1 = createButton(qandA.digitalclimatetest[qn].answers[0])
  button1.position(width/8,height/2+50);
  button1.mousePressed(changeValue1);
  button1.size(200, 100);

  button2 = createButton(qandA.digitalclimatetest[qn].answers[1]);
  button2.position(width/2.95,height/2+50);
  button2.mousePressed(changeValue2);
  button2.size(200, 100);

  button3 = createButton(qandA.digitalclimatetest[qn].answers[2]);
  button3.position(width/1.815,height/2+50);
  button3.mousePressed(changeValue3);
  button3.size(200, 100);

  button4 = createButton(qandA.digitalclimatetest[qn].answers[3]);
  button4.position(width/1.31,height/2+50);
  button4.mousePressed(changeValue4);
  button4.size(200, 100);
  pop()
}

function changeValue1(){

score = qandA.digitalclimatetest[qn].p1
poor[0].addToCcis(score)

  qn++
x=2
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
background(bg)
  setTimeout(loadGoodQuip,1000)
}

function changeValue2(){

score = qandA.digitalclimatetest[qn].p2
poor[0].addToCcis(score)
x=2
  qn++
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
background(bg)
  setTimeout(loadNeutralQuip,1000)
}

function changeValue3(){

score = qandA.digitalclimatetest[qn].p3
poor[0].addToCcis(score)
x=2
  qn++
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
background(bg)
  setTimeout(loadBadQuip,1000)
}

function changeValue4(){

score = qandA.digitalclimatetest[qn].p4
poor[0].addToCcis(score)
x=2
  qn++
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
background(bg)
  setTimeout(loadBadQuip,1000)
}


function loadGoodQuip(){
  rq = int(random(story.quips[0].good.length))
  let goodQuip = "story/" + story.quips[0].good[rq].audio + ".mp3"
  quip = loadSound(goodQuip,speakQuip)
}

function loadNeutralQuip(){
  rq = int(random(story.quips[0].neutral.length))
  let neutralQuip = "story/" + story.quips[0].neutral[rq].audio + ".mp3"
  quip = loadSound(neutralQuip,speakQuip)
}

function loadBadQuip(){
  rq = int(random(story.quips[0].bad.length))
  let badQuip = "story/" + story.quips[0].bad[rq].audio + ".mp3"
  quip = loadSound(badQuip,speakQuip)
}


function speakQuip(){
  quip.play()
  quip.onended(newQuestion)
}

function newQuestion(){
  if (qandA.digitalclimatetest.length >qn){
    setTimeout(loadFunFacts,2000);
  }else if(qandA.digitalclimatetest.length ==qn){
    setTimeout(loadEndings,2000)
}
}

function loadEndings(){
x=0
if (poor[0].ccis<=30){
  setTimeout(goodEnding,1000)
} else if(31<=poor[0].ccis && poor[0].ccis<=60){
  setTimeout(neutralEnding,1000)
}else if(poor[0].ccis>60){
  setTimeout(badEnding,1000)
}
}

function goodEnding(){
let goodE = "story/" + story.goodEnding[t].audio + ".mp3"
ending=loadSound(goodE,speakGoodEnding)
}

function neutralEnding(){
let neutralE = "story/" + story.neutralEnding[t].audio + ".mp3"
ending=loadSound(neutralE,speakNeutralEnding)
}

function badEnding(){
let badE = "story/" + story.badEnding[t].audio + ".mp3"
ending=loadSound(badE,speakBadEnding)
}

function speakGoodEnding(){
  background(0)
  ending.play()
  t++
  if(t<story.goodEnding.length){
  ending.onended(loadEndings)
}else{
  background(0)
  setTimeout(jobChoice,2000)
}
}

function speakNeutralEnding(){
  background(0)
  ending.play()
  t++
  if(t<story.neutralEnding.length){
  ending.onended(loadEndings)
}else{
  background(0)
  setTimeout(jobChoice,2000)
}
}

function speakBadEnding(){
  background(0)
  ending.play()
  t++
  if(t<story.badEnding.length){
  ending.onended(loadEndings)
}else{
  background(0)
  setTimeout(jobChoice,2000)
}
}


function jobChoice(){
  push()
  if (poor[0].ccis<=30){
    job1=story.jobs[0].goodjobs[0].job
  button1 = createButton(job1)
  } else if(31<=poor[0].ccis && poor[0].ccis<=60){
    job1=story.jobs[0].neutraljobs[0].job
  button1=  createButton(job1)
  }else if(poor[0].ccis>60){
    job1=story.jobs[0].badjobs[0].job
    button1= createButton(job1)
  }
  button1.position(width/8,height/2+50);
  button1.size(200, 100);
  button1.mousePressed(changeJob1);


  if (poor[0].ccis<=30){
    job2=story.jobs[0].goodjobs[1].job
    button2 = createButton(job2)
  } else if(31<=poor[0].ccis && poor[0].ccis<=60){
    job2=story.jobs[0].neutraljobs[1].job
    button2= createButton(job2)
  }else if(poor[0].ccis>60){
    job2=story.jobs[0].badjobs[1].job
    button2= createButton(job2)
  }
  button2.position(width/2.95,height/2+50);
  button2.size(200, 100);
  button2.mousePressed(changeJob2);


  if (poor[0].ccis<=30){
    job3=story.jobs[0].goodjobs[2].job
    button3= createButton(job3)
  } else if(31<=poor[0].ccis && poor[0].ccis<=60){
    job3=story.jobs[0].neutraljobs[2].job
    button3= createButton(job3)
  }else if(poor[0].ccis>60){
    job3=story.jobs[0].badjobs[2].job
    button3= createButton(job3)
  }
  button3.position(width/1.815,height/2+50);
  button3.size(200, 100);
  button3.mousePressed(changeJob3);


  if (poor[0].ccis<=30){
    job4=story.jobs[0].goodjobs[3].job
    button4= createButton(job4)
  } else if(31<=poor[0].ccis && poor[0].ccis<=60){
    job4=story.jobs[0].neutraljobs[3].job
    button4= createButton(job4)
  }else if(poor[0].ccis>60){
    job4=story.jobs[0].badjobs[3].job
    button4=  createButton(job4)
  }
  button4.position(width/1.31,height/2+50);
  button4.size(200, 100);
  button4.mousePressed(changeJob4);
  pop()
}

function changeJob1(){
  poor[0].addJob(job1)
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  housingChoice()
}

function changeJob2(){
  poor[0].addJob(job2)
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  housingChoice()
}

function changeJob3(){
  poor[0].addJob(job3)
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  housingChoice()
}

function changeJob4(){
  poor[0].addJob(job4)
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  housingChoice()
}


function housingChoice(){
  push()
  if (poor[0].ccis<=30){
    house1=story.housing[0].goodhousing[0].place
  button1 = createButton(house1)
  } else if(31<=poor[0].ccis && poor[0].ccis<=60){
    house1=story.housing[0].neutralhousing[0].place
  button1=  createButton(house1)
  }else if(poor[0].ccis>60){
    house1=story.housing[0].badhousing[0].place
    button1= createButton(house1)
  }
  button1.position(width/8,height/2+50);
  button1.size(200, 100);
  button1.mousePressed(changeHouse1);


  if (poor[0].ccis<=30){
    house2=story.housing[0].goodhousing[1].place
    button2 = createButton(house2)
  } else if(31<=poor[0].ccis && poor[0].ccis<=60){
    house2=story.housing[0].neutralhousing[1].place
    button2= createButton(house2)
  }else if(poor[0].ccis>60){
    house2=story.housing[0].badhousing[1].place
    button2= createButton(house2)
  }
  button2.position(width/2.95,height/2+50);
  button2.size(200, 100);
  button2.mousePressed(changeHouse2);


  if (poor[0].ccis<=30){
    house3=story.housing[0].goodhousing[2].place
    button3= createButton(house3)
  } else if(31<=poor[0].ccis && poor[0].ccis<=60){
    house3=story.housing[0].neutralhousing[2].place
    button3= createButton(house3)
  }else if(poor[0].ccis>60){
    house3=story.housing[0].badhousing[2].place
    button3= createButton(house3)
  }
  button3.position(width/1.815,height/2+50);
  button3.size(200, 100);
  button3.mousePressed(changeHouse3);


  if (poor[0].ccis<=30){
    house4=story.housing[0].goodhousing[3].place
    button4= createButton(house4)
  } else if(31<=poor[0].ccis && poor[0].ccis<=60){
    house4=story.housing[0].neutralhousing[3].place
    button4= createButton(house4)
  }else if(poor[0].ccis>60){
    house4=story.housing[0].badhousing[3].place
    button4=  createButton(house4)
  }
  button4.position(width/1.31,height/2+50);
  button4.size(200, 100);
  button4.mousePressed(changeHouse4);
  pop()
}


function changeHouse1(){
  poor[0].addHouse(house1)
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  endFinal()
}

function changeHouse2(){
  poor[0].addHouse(house2)
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  endFinal()
}

function changeHouse3(){
  poor[0].addHouse(house3)
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  endFinal()
}

function changeHouse4(){
  poor[0].addHouse(house4)
  button1.hide();
  button2.hide();
  button3.hide();
  button4.hide();
  endFinal()
}

function endFinal(){
  push()
  background(0)
  textSize(60)
  fill(255,255,255)
  textAlign(CENTER);
  text('Congratulations with your new job as ' + poor[0].job ,width/2,height/2)
  text('and your new housing at ' + poor[0].housing + ' !' ,width/2,height/2+100)
  pop()
}
