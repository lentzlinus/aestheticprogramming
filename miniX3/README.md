<h2>README for miniX3 by Linus Lentz</h2>

![](miniX3.gif)

Run miniX: https://lentz.gitlab.io/aestheticprogramming/miniX3/

Source code: https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX3/sketch.js

Image used: https://de.wikipedia.org/wiki/Electric_Ballroom#/media/Datei:Technoparty_Electric_Ballroom.jpg

Winnie Soon's original code: https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX3/original_throbber_by_siusoon.js


<h4>The assignment</h4>
For our third miniX we had to design our own throbber. I decided on a design that’s interactive.
My throbber does not move unless it is moved by the user. At first it seems counter intuitive.
The throbber does not spin, the user could think the page is not loading nor is it progressing.
But what would be the first reaction to that? Exactly, the user would move the mouse, revealing that the application or site is in fact loading. The background of people in a nightclub could already give a hint of what is coming.

The reason I chose to make the throbber interactive was that most throbber are not able to manipulated. I do still not understand why throbber only are designed this way. They are part of a waiting process, nothing else is happening on a screen beside them anyways, so why don’t people have fun with them. Actually I came across something that could explain why this is so. The videogame company Namco once filed a patent in 1995 on having minigames inside of loading screens, which first expired 2015. I can not imagine this patent was the reason for literally ALL loading screens being static in every kind of digital medium, but it might as well could be. Though I think that an interactive throbber, at least the way I implemented it, barely classifies as a minigame, I would rather call it a temporary distraction. Now that I think of it, the Google dinosaur minigame that starts when Google can’t connect to the internet sort of classifies, but it only appears on unexpected downtime, so I can’t really count it.


<h4>The Code</h4>
So for the code I made first time use of many functions and variables, but also brought
back some from my older miniXs. Starting from the top I declared some variables for then
three main color values: R, G, B. These I would link to the function colors() in which I would
assign different gains to each of them, to make different shades of colors over time.
The whole function was linked in an "If"-statement to the mouse being moved.
As in my other miniXs I loaded some media into the project, this time it was some people dancing in
a night club, an idea I first had after finishing the throbber itself, as it reminded me
of the fluorescent light sticks people often hold on raves. If you move the cursor fast enough
that impression is stronger. I changed the cursor with cursor('grab') to show the grab-icon,
further implying that a lightstick is being held. I connected the draw functions to the mouseMoved()
so that they only would be activated when the mouse is being moved.

The time-related code I used is „let num = 20;“, „let cir = 360/num*(frameCount%num)“ and „rotate(radians(cir));“, which I used because it was mentioned in the Aesthetic Programming course as an example, and because it makes the throbber spin on a circle, which I liked. As described on page 89-91 in “How humans and machines negotiate experience of time” by Hans Lammerant, machines do not experience time the same way as we humans do, which is based on recurring natural cycles like day and night as well as the four seasons of the year. As Lammerant describes on page 91-98, machines do compute time with help of a „real time clock“, short „RTC“ which runs on the cycles of the computers processor. These cycles are generally paced by evenly interrupts. The way this works in my miniX is, that instead of the a processor, the RTC is based on the frameCount, which also is evenly paced, that’s why it can be used instead. The 360 degrees of the circle are divided by the „num“-value I chose, and then multiplied with the division of frameCount and „num“ value. When the frameCount as well as the „num“-value change, time will go faster or slower accordingly.


<h4>Conclusion</h4>
Like I already mentioned, most throbber I encounter loop. They are either a line running in a circle or from side to side. Sometimes they also consist of images or icons. This is kind of bland in most cases. One way to characterize icons differently could be too make them more fitting contextually. For example when a bank application has a throbber, instead of having a bland one, it could show the different steps of processing occurring. This could easily be done visually with icons, like having a money icon wander in and out of different locations depending on the processing step. Something like this would make throbber part of the design rather than just a necessity.

This throbber kind of goes against how throbbers are meant to be used, but this is kind
of a theme with my miniXs. I like to challenge the proposition given and make something creative out of it.
On top of that I really enjoy to edit pre-existing code into how I would like things to look.
This is why I took Winnies code as a base, and I think now it looks nothing like it anymore, but helped me
start the process. Next miniX I will probably try to make something from scratch and see how that turns out.


Reference

Loading screen minigame patent:
https://patft.uspto.gov/netacgi/nph-Parser?d=PALL&p=1&u=%2Fnetahtml%2FPTO%2Fsrchnum.htm&r=1&f=G&l=50&s1=5,718,632.PN.&OS=PN/5,718,632&RS=PN/5,718,632

Google Dino game: https://dino-chrome.com/en

Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018), https://www.books.constantvzw.org/nl/home/tgso.
