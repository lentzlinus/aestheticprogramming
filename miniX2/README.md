<h2>README for miniX2 by Linus Lentz</h2>

![](showcase.png)

In this weeks assignment we were told to work with emoji and their cultural
aspects. I wanted to make this miniX interactive, as my last one was only visual.
As the way emojis are used today is a relatively young developemen, I thought
it would be fun and interesting to make the topic about anachronism.


<h4>What to make?...</h4>
I chose the painting "Ultima Cena", translates to "The last supper", by Juan de Juanes,
which depicts Jesus and his disciples gathering for Jesus' last meal before crucifixion.
I thought the painting would be a great template to experiment with. I wondered
how the painting could be altered by adding emojis. The first idea of course being
to replace their expressions with emojis, kind of like adding stamps to a postcard.

I stumbled across this very cool piece of code by user "maxremfort", which enabled
the kind of interaction I was thinking of. Using his code as a base, I added the painting
as a background, made my own emoji, and added 1 (actually 2) more basic ones.

<h4>The code</h4>
Well as said, I used code by the user "maxremfort" as a base to work off of. The bulk
off the code is the use of geometric forms like ellipse(), rect() and circle().
Other than that I imported the painting by declaring the variable "img" and Using
preload() for caching the image and then placing it in the "function setup" as img().
The most complicated part is that of tracking the mouse cursor and placing an emoji
at its location when clicked. The code by maxremfort made this no problem, but I ended
up optimizing and deleting parts of it, that seemed to be unused in the first place.
The mouse i tracked with pmouseX() and pmouseY() and the mouse click placement is made possible
by using the function mouseClicked() and then determining which button is pressed making an
if statement if (mouseButton === RIGHT/MIDDLE/CENTER) emoji1/2/3(true,true). My guess
for there being to points of activation is that there are two axes, but I am not 100% sure.
Other than that I have fully understood maxremforts code.

<h4>The emoji</h4>
So my emoji is yellow with two eyes, eyebrows and a mouth. The emoji is smiling and looking right outside of the screen. My intent was to make a neutral emoji, but I think it turned out looking more male than female, which I think is due to the lack of any hair. I would say my emoji does not fulfill the general rule applied to humanoid emojis since 2015 where Apple changed theirs to include different skin tones, which is described in "Modifying the Universal" by Roel Roscam Abbing, Peggy Pierrot and Femke Snelting. To add color options like the "Fitzpatrick scale" described in the aforementioned text could have been done to further inclusion. Though this demands in the first place that people identify with my emoji. To be honest I think I prefer Google's old blob style emoji shown on figure 4, p.41 in the text the most, as it makes the emoji more abstract and less likely for people to identify themselves through them. I find it very interesting though that google made this change from the before used emojis with only white skin color. That was a clear case of exclusion, as the emojis also were more humanoid.

<h4>Verdict</h4>
The emoji I made ended up looking kind of smug, but also confused and maybe astonished
at the same time. I like it quite a lot, as I don't know of an emoji that shares the same expression.
As for the other emoji I chose the jolly one with the halo above its head, which is often
used for things relating to kindness, faith, wishing and christianity as a whole.
I think it is a perfect fit, and gives us the interesting question of what Jesus would
have thought about christianity being represented this way nowadays. Or even more
interestingly what the painter Juan de Juanes would have thought about it, as he
himself chose to depict Jesus in a certain way in his pictures. One glaring similarity
being that he added halos to all of the characters in the painting. On top of that Jesus
is holding a wafer which had the same form as an emoji. Coincidence? I think not.

As a third option I added the widely used "crying and laughing" emoji. I feel quite
the culture shock seeing this one united with this piece of art. In my opinion
it is kind of mocking and/or parodying the painting when it is used. But one could argue
the other emojis do the same thing, maybe even more so as one even shares the pictures
theme on a contextual level.

Link to working executable in web browser (use Chrome or Firefox): https://lentz.gitlab.io/aestheticprogramming/miniX2/

Link to the source code: https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX2/sketch.js

Reference

Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, “Modifying the Universal,” in Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, eds., Executing Practices (London: Open Humanities Press, 2018), 35-51, http://www.data-browser.net/db06.html.

Link to maxremfort's code i borrowed: https://editor.p5js.org/maxremfort/sketches/hqxwqoF-w

Source of the painting by Juan de Juanes: https://commons.wikimedia.org/wiki/File:Última_Cena_-_Juan_de_Juanes.jpg
