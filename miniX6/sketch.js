//establishing the base for the classes
let mario = [];
let goomba = [];
let hat = [];

//importing all assets
function preload() {
  backdrop = loadImage('sprites/backdrop.png');
  win = loadImage('sprites/win.png');
  loose = loadImage('sprites/loose.png');
  baldMario = loadImage('sprites/baldMario.png');
  deadMario = loadImage('sprites/deadMario.png');
  hat = loadImage('sprites/hat.png');
  goomba = loadImage('sprites/goomba.png');
  hatMario = loadImage('sprites/hatMario.png');
  castle = loadSound('sounds/castle.mp3');
  success = loadSound('sounds/success.mp3');
  gameover = loadSound('sounds/gameover.mp3');
}

//creating the canvas, importing the background, starting the music and establishing the classes
function setup(){
  createCanvas(1000, 600);
  castle.play();
  mario[0] = new Mario(100, 100);
  goomba[0] = new Goomba(900, 500);
  hat[0] = new Hat(900, 500);
}

function draw(){
background(backdrop);

//establishing the classes abilities and behaviours
mario[0].showMario();
mario[0].moveMario();
goomba[0].showGoomba();
goomba[0].moveGoomba();
hat[0].showHat();
hat[0].moveHat();

}

//constructing the class information for class Hat
class Hat{
  constructor(_xpos, _ypos){
    this.xpos = _xpos
    this.ypos = _ypos
  }

showHat(){
  image(hat, this.xpos, this.ypos)
}

moveHat(){
  this.xpos = this.xpos + 0.015;
  this.ypos = this.ypos + 0.015;
  this.xpos = noise(this.xpos) * width;
  this.ypos = noise(this.ypos) * height;
  }
}

//constructing the class information for class Goomba
class Goomba{
  constructor(_xpos, _ypos){
    this.xpos = _xpos
    this.ypos = _ypos
  }

showGoomba(){
  image(goomba, this.xpos, this.ypos)
}

moveGoomba(){
  this.xpos = this.xpos + 0.015;
  this.ypos = this.ypos + 0.015;
  this.xpos = noise(this.xpos) * width;
  this.ypos = noise(this.ypos) * height;
  }
}

//constructing the class information for class Mario
class Mario{
  constructor(_xpos, _ypos){
    this.xpos = _xpos
    this.ypos = _ypos
  }

showMario(){
  image(baldMario, this.xpos, this.ypos)
}

//making mario able to be controlled
moveMario(){
  if (keyIsDown(LEFT_ARROW)) {
    this.xpos -= 5;
  }

  if (keyIsDown(RIGHT_ARROW)) {
    this.xpos += 5;
  }

  if (keyIsDown(UP_ARROW)) {
    this.ypos -= 5;
  }

  if (keyIsDown(DOWN_ARROW)) {
    this.ypos += 5;
  }
  if (keyIsDown(65)) {
    this.xpos -= 5;
  }

  if (keyIsDown(68)) {
    this.xpos += 5;
  }

  if (keyIsDown(87)) {
    this.ypos -= 5;
  }

  if (keyIsDown(83)) {
    this.ypos += 5;
  }
  }
}
